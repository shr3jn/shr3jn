FORMAT: 1A

# HospitalManagementAPI

# Users [/users]
Users

## Normal Login [POST /users/login]
Normal login based on email address and password

+ Request (application/json)
    + Body

            {
                "email": "foo@bar.com",
                "password": "foobar"
            }

+ Response 200 (application/json)
    + Body

            {
                "token": "hJbninpoxj4nfdsnnnkbhdd"
            }

## Logout [POST /users/logout]
Logout

+ Request (application/json)
    + Headers

            Authorization: Bearer <TOKEN>
    + Body

            []

+ Response 200 (application/json)
    + Body

            {
                "message": "You have been logged out."
            }

## User Registration [POST /users]
User Registration

+ Request (application/json)
    + Body

            {
                "name": "Shreejan Shrestha",
                "email": "foo@bar.com",
                "address": "Kathmandu",
                "phone": "9800000000",
                "age": 23,
                "gender": "Male",
                "password": "foobar"
            }

+ Response 200 (application/json)
    + Body

            {
                "message": "Registration successful"
            }

# Banners [/banners]
Banners

## Banners [GET /banners]
Get list of all banners

+ Response 200 (application/json)
    + Body

            {
                "banners": [
                    {
                        "id": 1,
                        "link": "http://#",
                        "caption": null,
                        "createdAt": "2020-05-23T00:00:00.000000Z",
                        "updatedAt": "2020-05-23T00:00:00.000000Z"
                    }
                ]
            }

# Hospitals [/hospitals]
Hospitals

## All [GET /hospitals]
Get a JSON representation of all the hospitals.

+ Response 200 (application/json)
    + Body

            {
                "hospitals": [
                    {
                        "id": 1,
                        "name": "demo Hospital",
                        "createdAt": "2020-05-23T00:00:00.000000Z",
                        "updatedAt": "2020-05-23T00:00:00.000000Z"
                    }
                ]
            }

## Create [POST /hospitals]
Create new hospital

+ Request (application/json)
    + Body

            {
                "name": "foo"
            }

+ Response 200 (application/json)
    + Body

            {
                "message": "Hospital created."
            }

## Read [GET /hospitals/{id}]
Get a JSON representation a hospital by id

+ Response 200 (application/json)
    + Body

            {
                "hospital": {
                    "id": 1,
                    "name": "demo Hospital",
                    "createdAt": "2020-05-23T00:00:00.000000Z",
                    "updatedAt": "2020-05-23T00:00:00.000000Z"
                }
            }

## Update [PUT /hospitals]
Update existing hospital

+ Request (application/json)
    + Body

            {
                "name": "foo"
            }

+ Response 200 (application/json)
    + Body

            {
                "message": "Hospital updated."
            }

## Delete [DELETE /hospitals/{id}]
Delete a hospital by id

+ Response 204 (application/json)